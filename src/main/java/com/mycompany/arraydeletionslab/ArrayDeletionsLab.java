/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.arraydeletionslab;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class ArrayDeletionsLab {
    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            return arr;
        }
        
        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        
        arr[arr.length - 1] = 0;
        
        return arr;
    }
    
    public static int[] deleteElementByValue(int[] arr, int value) {
        int index = -1;
        
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                index = i;
                break;
            }
        }
        
        if (index == -1) {
            return arr;
        }
        
        return deleteElementByIndex(arr, index);
    }
    
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        
        System.out.println("Original Array: " + Arrays.toString(arr));
        
        int[] updatedArray1 = deleteElementByIndex(arr, 2);
        System.out.println("Array after deleting element at index 2: " + Arrays.toString(updatedArray1));
        
        int[] updatedArray2 = deleteElementByValue(arr, 4);
        System.out.println("Array after deleting element with value 4: " + Arrays.toString(updatedArray2));
    }
}

